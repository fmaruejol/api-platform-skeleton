# [API Platform](https://api-platform.com/) Project Skeleton

This project is a skeleton to create API with [API Platform](https://api-platform.com/).

## What features comes with the skeleton ?

* CRUD Endpoint for manipulate users
* JWT Authentication
* An admin available at /admin

> You should create a user manually or with the given data fixtures to get a JWT token or access the admin

## Get It Started

#### Clone project and install dependencies:

```bash
$ git clone https://gitlab.com/fmaruejol/api-platform-skeleton.git
$ cd api-platform-skeleton
$ composer install
$ touch .env.local
```

#### Generate SSH keys for the JWT token:

```bash
$ openssl genpkey -out config/jwt/private.pem -aes256 -algorithm rsa -pkeyopt rsa_keygen_bits:4096
$ openssl pkey -in config/jwt/private.pem -out config/jwt/public.pem -pubout
```

#### Edit the .env.local file:

```bash
APP_ENV=dev
APP_SECRET=ec46ff51ba82cb7205eea2a45eb9e36f
DATABASE_URL=mysql://user:password@127.0.0.1:3306/database?serverVersion=5.7
JWT_PASSPHRASE=MyPassphrase
```

#### Create your database:

```bash
$ php bin/console doctrine:database:create
$ php bin/console doctrine:migration:migrate
```

#### Load data fixtures (optionnal)
```bash
$ php bin/console doctrine:fixtures:load
```

This create an admin user with the following credentials `admin/foo`

Your app is ready !

## Usage

##### API Documentation (OpenAPI)

API documentation accessible at `/api/docs`.
> To be able to call all endpoints with no restriction the user should have the
role `ROLE_API_ADMIN`

##### Admin

Admin is accessible at `/admin`.
> To be able to access the admin the user should have role `ROLE_API_ADMIN`.

