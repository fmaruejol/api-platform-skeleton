const authProvider = {
    login: ({username, password}) => {
        const request = new Request(`${window.location.protocol}//${window.location.hostname}:${window.location.port}/api/token`, {
            method: 'POST',
            body: JSON.stringify({username, password}),
            headers: new Headers({'Content-Type': 'application/json'}),
        });
        let jwtToken = null;

        return fetch(request)
            .then(response => {
                if (response.status < 200 || response.status >= 300) {
                    throw new Error(response.statusText);
                }
                return response.json();
            })
            .then(({token}) => {
                const getUserRequest = new Request(`${window.location.protocol}//${window.location.hostname}:${window.location.port}/api/users/me`, {
                    method: 'GET',
                    headers: new Headers({'Content-Type': 'application/json', 'Authorization': `Bearer ${token}`}),
                });

                jwtToken = token;

                return fetch(getUserRequest);
            })
            .then(response => {
                if (response.status < 200 || response.status >= 300) {
                    throw new Error(response.statusText);
                }
                return response.json();
            })
            .then(user => {
                if (!user.roles || !user.roles.includes('ROLE_API_ADMIN')) {
                    throw new Error('User don\'t have permissions to access admin');
                }

                localStorage.setItem('token', jwtToken);

                return jwtToken;
            });
    },
    logout: () => {
        localStorage.removeItem('token');
        return Promise.resolve();
    },
    checkError: (error) => {
        const status = error.status;
        if (status === 401 || status === 403) {
            localStorage.removeItem('token');
            return Promise.reject();
        }
        return Promise.resolve();
    },
    checkAuth: () => localStorage.getItem('token')
        ? Promise.resolve()
        : Promise.reject(),
    getPermissions: params => Promise.resolve(),
};

export default authProvider;