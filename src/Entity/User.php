<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\API\GetTokenUser;
use App\Controller\API\GetUserFromToken;
use App\Repository\UserRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 *
 * @ApiResource(
 *   collectionOperations={
 *     "get"={"security"="is_granted('ROLE_API_ADMIN')"},
 *     "post"={"validation_groups"={"Default", "create"}, "security"="is_granted('ROLE_API_ADMIN')"}
 *   },
 *   itemOperations={
 *     "get"={"requirements"={"id"="\d+"}, "security"="is_granted('ROLE_API_ADMIN') or object.getId() == user.getId()"},
 *     "put"={"validation_groups"={"Default", "update"}, "security"="is_granted('ROLE_API_ADMIN') or object.getId() == user.getId()"},
 *     "delete"={"security"="is_granted('ROLE_API_ADMIN')"},
 *     "get_user_from_token"={
 *       "method"="GET",
 *       "path"="/users/me",
 *       "controller"=GetUserFromToken::class,
 *       "read"=false
 *     }
 *   },
 *   normalizationContext={"groups"={"read"}},
 *   denormalizationContext={"groups"={"write"}}
 * )
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"read", "write"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=30, unique=true)
     * @Groups({"read", "write"})
     * @Assert\NotBlank
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     * @Groups({"read", "write"})
     * @Assert\Email
     * @ApiProperty(iri="http://schema.org/email")
     */
    private $email;

    /**
     * @var string[]
     *
     * @ORM\Column(type="json")
     * @Groups({"read", "write"})
     * @ApiProperty(
     *     jsonldContext={
     *         "@type"="http://www.w3.org/2001/XMLSchema#array"
     *     }
     * )
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @var string
     * @Groups({"write"})
     * @ApiProperty(required=false)
     * @Assert\NotBlank(groups={"create"})
     */
    private $plainPassword;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): ?string
    {
        return (string) $this->username;
    }

    /**
     * @param string $username
     */
    public function setUsername(string $username): void
    {
        $this->username = $username;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }

    /**
     * @param string $plainPassword
     */
    public function setPlainPassword(string $plainPassword): void
    {
        $this->plainPassword = $plainPassword;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
    }
}
