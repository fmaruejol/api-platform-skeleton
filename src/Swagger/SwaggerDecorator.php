<?php

namespace App\Swagger;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

final class SwaggerDecorator implements NormalizerInterface
{
    private $decorated;

    public function __construct(NormalizerInterface $decorated)
    {
        $this->decorated = $decorated;
    }

    public function supportsNormalization($data, string $format = null): bool
    {
        return $this->decorated->supportsNormalization($data, $format);
    }

    public function normalize($object, string $format = null, array $context = [])
    {
        $docs = $this->decorated->normalize($object, $format, $context);

        $docs['info']['title'] = 'Skeleton API';
        $docs['info']['description'] = 'A Skeleton API';
        $docs['info']['version'] = '0.0.1';

        $userMeDocumentation = $docs['paths']['/api/users/me']['get'];
        $userMeDocumentation['parameters'] = array_values(
            array_filter(
                $userMeDocumentation['parameters'],
                function ($param) {
                    return $param['name'] !== 'id';
                }
            )
        );
        $userMeDocumentation['summary'] = 'Retrieves the User resource from the token';
        $docs['paths']['/api/users/me']['get'] = $userMeDocumentation;

        return array_merge_recursive(
            $docs,
            $this->getTokenDocumentation()
        );
    }

    private function getTokenDocumentation(): array
    {
        $docs['components']['schemas']['Token'] = [
            'type'       => 'object',
            'properties' => [
                'token' => [
                    'type'     => 'string',
                    'readOnly' => true,
                ],
            ],
        ];

        $docs['components']['schemas']['Credentials'] = [
            'type'       => 'object',
            'properties' => [
                'username' => [
                    'type'    => 'string',
                    'example' => 'foo',
                ],
                'password' => [
                    'type'    => 'string',
                    'example' => 'bar',
                ],
            ],
        ];

        $docs['paths'] = [
            '/api/token' => [
                'post' => [
                    'tags'        => ['Token'],
                    'operationId' => 'postCredentialsItem',
                    'summary'     => 'Get JWT token to login.',
                    'security' => [],
                    'requestBody' => [
                        'description' => 'Create new JWT Token',
                        'content'     => [
                            'application/json' => [
                                'schema' => [
                                    '$ref' => '#/components/schemas/Credentials',
                                ],
                            ],
                        ],
                    ],
                    'responses'   => [
                        Response::HTTP_OK => [
                            'description' => 'Get JWT token',
                            'content'     => [
                                'application/json' => [
                                    'schema' => [
                                        '$ref' => '#/components/schemas/Token',
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];

        $docs['components']['securitySchemes']['token']['type'] = 'http';
        $docs['components']['securitySchemes']['token']['scheme'] = 'bearer';
        $docs['components']['securitySchemes']['token']['name'] = 'Authorization';
        $docs['components']['securitySchemes']['token']['description'] = 'Token identifying a user. You can get one with token endpoint.';
        $docs['components']['securitySchemes']['token']['bearerFormat'] = 'JWT';
        $docs['security'][] = ['token' => []];

        return $docs;
    }
}