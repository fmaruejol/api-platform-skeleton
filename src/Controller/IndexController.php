<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController
{
    /**
     * @Route("/admin", name="index")
     *
     * @return Response
     */
    public function admin(): Response
    {
        return $this->render('pages/admin.html.twig');
    }
}
