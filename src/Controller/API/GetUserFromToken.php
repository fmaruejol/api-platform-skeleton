<?php

namespace App\Controller\API;

use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class GetUserFromToken
{
    private $tokenStorage;

    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    public function __invoke(?User $data): User
    {
        $data = $this->tokenStorage->getToken()->getUser();

        return $data;
    }
}
